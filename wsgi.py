
import os
import sys

from django.core.wsgi import get_wsgi_application

thisdir = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = os.path.join(thisdir, 'zurl')
settings_module = "%s.settings" % PROJECT_ROOT.split(os.sep)[-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

# Django don't serve static files ----
#application = get_wsgi_application()

# Django serve static files ----
from dj_static import Cling
application = Cling(get_wsgi_application())
