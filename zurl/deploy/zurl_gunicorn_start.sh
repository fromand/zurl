#!/bin/bash

NAME="ZURL"                                  # Name of the application
DJANGODIR=/home/djapp/webroot/             # Django project directory
SOCKFILE=/home/djapp/run/gunicorn.sock  # we will communicte using this unix socket
USER=djapp                                        # the user to run as
GROUP=djapp                                     # the group to run as
NUM_WORKERS=2                                     # how many worker processes should Gunicorn spawn
MAX_REQUESTS=128                             # Max requests before worker restart
DJANGO_SETTINGS_MODULE=zurl.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source ../.virtualenvs/zurl/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --max-requests $MAX_REQUESTS \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=info \
  --log-file=-
