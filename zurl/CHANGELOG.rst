Version 1.2.x
-------------
- Contrôle des domaines bannis avant génération d'un raccourci
- Contrôle des identifiants utilisateurs bannis avant génération d'un raccourci
- Consultation des stats d'un raccourci
- Mise en place d'une API ouverte (création + interro stats)


Version 1.1.2
-------------
- Correction routage des raccourcis personnalisés


Version 0.9.n (Beta)
-------------

- Requête anonyme de réduction d'URL
- Raccourci personnalisable
- Copier (web) de l'URL raccourcie (voir todo)
- Quota controlé par adresse IP, cookie uuid ou username
- Traçabilité des requêtes de raccourci
- Traçabilité des redirections
- statistiques globales en bas de page d'index


Version 1.0.0
-------------

- Récupération du titre de la page raccourci
- Lister les derniers raccourcis non privés créés sur page d'accueil
(raccourci + titre page cible)
- Création d'un QRCode à partir d'un raccourci
- création rapide d'un raccourci via bookmark (voir todo)
(l'appel du site depuis une page externe créé un raccourci pour cette
page)
