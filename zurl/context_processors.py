#-*- coding: utf-8 -*-

import platform

import django

from __init__ import __application_info__

def app_infos(request):
    return { 'application_info' : __application_info__,
             'platform_info' : {
                    'django': {
                        'version': u"%d.%d.%d" % ( django.VERSION[0],
                                                django.VERSION[1],
                                                django.VERSION[2]),
                        },
                    'python': {
                        'version': platform.python_version(),
                        },
                    'system': platform.system(),
            },
    }

def http_request(request):
    return {    'is_ajax': request.is_ajax(),
                'is_secure': request.is_secure() }
