#-*- coding: utf-8 -*-

import logging
import mimetypes
import urlparse

from bs4 import BeautifulSoup
import requests

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

from quota.models import QuotaCounter

from shorturl import encode, enbase


log = logging.getLogger('zurl_api')


class ZURL(models.Model):
    """ Table des couples URL/raccourci
    """

    REDIRECTION_TYPE_CHOICES = (
        ('302', 'temporary'),
        ('301', 'permanent'),
    )

    url = models.URLField(
        max_length=1024, unique=True, db_index=True,
        verbose_name=u"URL")

    title = models.CharField(
        max_length=256, null=True, blank=True,
        verbose_name=u"url title", default=None)

    key = models.SlugField(
        max_length=32, db_index=True, null=True, blank=True,
        verbose_name=u"key")

    redirection_type = models.CharField(
        max_length=3,
        verbose_name=u"redirection type",
        choices=REDIRECTION_TYPE_CHOICES, default='302')
    private = models.BooleanField(
        verbose_name=u'private', null=False,
        blank=True, default=False)

    password_sha1 = models.CharField(
        max_length=32, null=True, blank=True,
        verbose_name=u"password")
    expiration_time = models.DateTimeField(
        blank=True, null=True,
        verbose_name=u"expiration time", default=None)
    usage_max = models.IntegerField(verbose_name=u"usage max", default=0)

    created_IP = models.CharField(
        max_length=128, db_index=True,
        verbose_name=u"IP source", default="127.0.0.1")
    created_user = models.ForeignKey(
        User, null=True, blank=True, db_index=True,
        related_name="zurlowner_related",
        verbose_name=u"owner")
    created_ref = models.CharField(
        max_length=128, db_index=True, null=True, blank=True,
        verbose_name=u"source ref")
    created_time = models.DateTimeField(
        auto_now_add=True,
        verbose_name=u"creation time")
    used_count = models.IntegerField(verbose_name=u"usage counter", default=0)
    used_last = models.DateTimeField(
        auto_now=True, auto_now_add=True,
        verbose_name=u"last used time")

    class Meta:
        verbose_name_plural = u"zURLs"
        verbose_name = u"zURL"
        ordering = ["key", ]

    @property
    def is_anonymous(self):
        return self.created_user is None

    @property
    def is_timeout(self):
        if not self.expiration_time is None:
            return timezone.now() >= self.expiration_time
        return False

    @property
    def is_over(self):
        if self.usage_max > 0:
            return self.used_count > self.usage_max
        return False

    @property
    def is_protected(self):
        if self.password_sha1 is None or len(self.password_sha1.strip()) == 0:
            return False
        return True

    def __unicode__(self):
        return u"%s" % (self.key)

    def save(self, *args, **kwargs):
        if (self.key is None or len(self.key.strip()) == 0):
            if self.pk is None:
                super(ZURL, self).save(*args, **kwargs)
            b = encode(self.id)
            self.key = enbase(b)
            log.debug(u"new zurl created. key: %s -> %s" % (
                self.key, self.url))
        super(ZURL, self).save(*args, **kwargs)

    @staticmethod
    def get(key, password_sha1=None):
        try:
            z = ZURL.objects.get(key=key)
            if z.is_protected:
                if not z.password_sha1 == password_sha1:
                    log.debug(u"bad password for key %s" % (z.key))
                    raise ZURL.DoesNotExist
            if z.is_timeout:
                log.debug(u"timeout for key %s" % (z.key))
                raise ZURL.DoesNotExist
            if z.is_over:
                log.debug(u"key %s limit is over" % (z.key))
                raise ZURL.DoesNotExist
            z.used_count += 1
            z.used_last = timezone.now()
            z.save()
            URL = z.url
            redirection_type = z.redirection_type
        except ZURL.DoesNotExist:
            z = None
            URL = None
            redirection_type = None
        return (z, URL, redirection_type)

    @staticmethod
    def set(
            URL,
            request=None,
            key=None,
            expiration_time=None,
            password_sha1=None,
            private=None,
            redirection_type=None,
            saveit=True):

        if key is not None:
            try:
                z = ZURL.objects.get(key=key)
                return None
            except ZURL.DoesNotExist:
                pass

        try:
            z = ZURL.objects.get(url=URL)
        except ZURL.DoesNotExist:

            z = ZURL()
            z.url = URL

            if key is not None:
                z.key = key

            if not request is None:
                if not request.user.is_anonymous():
                    z.created_user = request.user

            if not expiration_time is None:
                z.expiration_time = expiration_time

            if not password_sha1 is None:
                z.password_sha1 = password_sha1

            if not private is None:
                z.private = private

            if not redirection_type is None:
                z.redirection_type = redirection_type

            z.created_IP = QuotaCounter._getIP(request)
            z.created_ref = QuotaCounter._getkey(request)

            urlsplitted = urlparse.urlsplit(z.url)
            urlfilename = urlsplitted.path.split('/')[-1]
            mt = mimetypes.guess_type(urlfilename)[0]
            if (mt is None) or (mt == 'text/html'):
                req = requests.get(z.url)
                req_encoding = "utf-8"
                if not req.apparent_encoding is None:
                    req_encoding = req.apparent_encoding
                elif not req.encoding is None:
                    req_encoding = req.encoding
                soup = BeautifulSoup(req.content.decode(req_encoding))
                z.title = soup.title.string
            else:
                z.title = u"file: %s" % (urlfilename)

            if saveit:
                z.save()

        return z.key


class BlackDomain(models.Model):
    """ Blacklisted domain
    """

    domain = models.CharField(
        max_length=128, db_index=True, verbose_name=u"domain")

    class Meta:
        verbose_name_plural = u"blacklisted domains"
        verbose_name = u"blacklisted domain"
        ordering = ["domain", ]

    @staticmethod
    def is_blacklisted(url):
        hostname = urlparse.urlparse(url).hostname
        blacks = BlackDomain.objects.filter(domain__icontains=hostname)
        if blacks.count() > 0:
            return True
        return False


class RedirectLogs(models.Model):
    """ Traçabilité des redirections effectuées
    """

    zurl = models.ForeignKey(
        ZURL, db_index=True, related_name="zurl_related",
        verbose_name=u"zurl")
    timestamp = models.DateTimeField(
        auto_now_add=True, db_index=True,
        verbose_name=u"creation time")
    source_IP = models.CharField(
        max_length=128, db_index=True,
        verbose_name=u"IP source", default="127.0.0.1")

    class Meta:
        verbose_name_plural = u"logs"
        verbose_name = u"log"
        ordering = ["timestamp", ]

    @staticmethod
    def add(zurl, source):
        log = RedirectLogs()
        log.zurl = zurl
        log.source_IP = source
        log.save()
