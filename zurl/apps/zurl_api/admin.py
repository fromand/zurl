#-*- coding: utf-8 -*-

from django.contrib import admin

from models import *


class ZURLAdmin(admin.ModelAdmin):

    list_display = [
        'id', 'key', 'url', 'title', 'private', 'redirection_type',
        'created_user', 'expiration_time',
        'usage_max', 'used_count', 'created_IP', 'created_ref',
        'created_time', 'used_last',
    ]
    list_display_links = ['id', 'key', 'url', ]
    search_fields = ['url', 'key', 'created_user', 'created_IP', 
        'created_ref', 'title']
    list_filter = ['private', 'redirection_type', ]
    ordering = ['-id', ]
    date_hierarchy = 'created_time'
    actions = ['delete_selected']
    readonly_fields = (
        "created_time", "created_user", 'created_IP', 'created_ref',
        "used_count", "used_last", "key", 'title',
    )

    fieldsets = (
        (
            None, {
                'classes': ('wide', ),
                'fields': ('url',
                            'title',
                           ('key', 'redirection_type', 'private', ), ),
            }
        ),
        (
            'Advanced options', {
                'classes': ('collapse', ),
                'fields': ('usage_max', 'expiration_time', 'password_sha1', ),
            }
        ),
        (
            'Meta', {
                'classes': ('collapse', ),
                'fields': (
                    ('created_user', 'created_time', 'created_IP',
                        'created_ref',),
                    ('used_last', 'used_count', ),
                ),
            }
        ),
    )

    def save_model(self, request, obj, form, change):
        obj.save()



class BlackDomainAdmin(admin.ModelAdmin):

    list_display = ['id', 'domain', ]
    list_display_links = ['id', ]
    search_fields = ['domain', ]
    ordering = ['domain', ]


class RedirectLogsAdmin(admin.ModelAdmin):

    list_display = ['id', 'zurl', 'timestamp', 'source_IP', ]
    list_display_links = ['id', ]
    search_fields = ['zurl', 'source_IP', ]
    ordering = ['-timestamp', ]
    date_hierarchy = 'timestamp'
    readonly_fields = ('zurl', 'timestamp', 'source_IP', )


admin.site.register(ZURL, ZURLAdmin)
admin.site.register(BlackDomain, BlackDomainAdmin)
admin.site.register(RedirectLogs, RedirectLogsAdmin)
