#-*- coding: utf-8 -*-

import datetime
import json
import logging
import os
import requests
import time
import uuid

import qsstats

from models import *
from quota.models import *

from django.contrib import messages
from django.db.models import Max
from django.http import Http404, HttpResponse
from django.shortcuts import redirect, render_to_response
from django.template import RequestContext
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.utils.text import slugify
from django.views.decorators.cache import never_cache

log = logging.getLogger('zurl_api')


def _set_browser_cookies(request, response, rollup5=False):

    expires = datetime.datetime.strftime(
        datetime.datetime.utcnow() + datetime.timedelta(
            seconds=settings.ZURL_COOKIE_MAX_AGE),
        "%a, %d-%b-%Y %H:%M:%S GMT"
    )

    if request.COOKIES.get(settings.ZURL_COOKIE_BROWSER_UUID, None) is None:
        response.set_cookie(
            settings.ZURL_COOKIE_BROWSER_UUID,
            uuid.uuid1().hex,
            settings.ZURL_COOKIE_MAX_AGE,
            expires
        )
        response.set_cookie(
            settings.ZURL_COOKIE_FIRST_USE,
            datetime.datetime.strftime(
                datetime.datetime.utcnow(),
                "%a, %d-%b-%Y %H:%M:%S GMT"
            ),
            settings.ZURL_COOKIE_MAX_AGE,
            expires
        )

    response.set_cookie(
        settings.ZURL_COOKIE_LAST_USE,
        datetime.datetime.strftime(
            datetime.datetime.utcnow(),
            "%a, %d-%b-%Y %H:%M:%S GMT"
        ),
        settings.ZURL_COOKIE_MAX_AGE,
        expires
    )

    if request.COOKIES.get(settings.ZURL_COOKIE_PREFERENCES, None) is None:
        response.set_cookie(
            settings.ZURL_COOKIE_PREFERENCES,
            json.dumps({
                'preview_before_redirect': True,
            }),
            settings.ZURL_COOKIE_MAX_AGE,
            expires
        )

    if rollup5:
        if request.COOKIES.get(settings.ZURL_COOKIE_ROLLUP5, None) is None:
            cookievalue = 1
        else:
            cookievalue = int(request.COOKIES.get(settings.ZURL_COOKIE_ROLLUP5)) + 1
            if cookievalue > settings.ZURL_COOKIE_ROLLUP5_MAX:
                cookievalue = 1

        response.set_cookie(
            settings.ZURL_COOKIE_ROLLUP5,
            str(cookievalue),
            settings.ZURL_COOKIE_MAX_AGE,
            expires
        )

    return response


def home_view(request, zurl=None, base_url=None, gakey=None):
    request.session.set_test_cookie()
    response = render_to_response(
        "index.html",
        {
            'zurl': zurl,
            'base_url': base_url,
            'site': Site.objects.get(id=settings.ZURL_REDIRECT_SITE_ID),
            'ga_key': gakey,
        },
        context_instance=RequestContext(request))
    return _set_browser_cookies(request, response)


def tops_view(request):
    request.session.set_test_cookie()
    response = render_to_response(
        "tops.html",
        {
            'site': Site.objects.get(id=settings.ZURL_REDIRECT_SITE_ID),
        },
        context_instance=RequestContext(request))
    return _set_browser_cookies(request, response)


def anonymous_url_set(request, direct=False):
    """ Requête anonyme pour obtenir une adresse raccourcie
    """

    if request.session.test_cookie_worked():
        request.session.delete_test_cookie()
    elif request.COOKIES.get(settings.ZURL_COOKIE_BROWSER_UUID, None) is None:
        messages.add_message(
            request, messages.WARNING,
            u'''Pour profiter de l'ensemble des fonctionnalités du site,
            il est préférable d'autoriser l'usage des cookies et de disposer
            d'un navigateur récent.'''
        )
    quota_counter = QuotaCounter.set(
        settings.ZURL_ANONYMOUS_REQUEST_QUOTA_NAME,
        request
    )
    zurl = None
    base_url = z_object = None
    if quota_counter.check():
        if not direct:
            base_url = request.POST['base_url']
        else:
            base_url = request.GET['base_url']
        validator = URLValidator()
        try:
            notvalid = False
            validator(base_url)
        except ValidationError:
            notvalid = True
        if not notvalid and not BlackDomain.is_blacklisted(base_url):
            base_key = ''
            if not direct:
                base_key = request.POST['base_key']
            if len(base_key.strip()) == 0:
                base_key = None
            else:
                base_key = slugify(base_key)
            url_key = ZURL.set(base_url, request=request, key=base_key)
            if not url_key is None:
                current_site = Site.objects.get(id=settings.ZURL_REDIRECT_SITE_ID)
                log.info(u"[%s][k:%s] new key -> %s" % (
                    QuotaCounter._getIP(request), url_key, base_url))
                zurl = "http://%s/%s" % (current_site.domain, url_key)
                z_object = ZURL.objects.get(key=url_key)
                #update_site_preview(url_key)  # TODO: rendre asynchrone (via Celery ?)
                #update_site_QRcode(url_key)   # TODO: rendre asynchrone (via Celery ?)
            else:
                messages.add_message(
                    request, messages.WARNING,
                    u'Impossible de créer votre raccourci !'
                )
        else:
            messages.add_message(
                request, messages.WARNING,
                u'Impossible de créer votre raccourci : adresse non valide !'
            )
    else:
        log.warning(u"[%s] quota exceed" % (QuotaCounter._getIP(request)))
        messages.add_message(
            request, messages.ERROR,
            u'Vous avez dépassé votre quota : essayez de nouveau plus tard...')

    response = render_to_response(
        "index.html",
        {
            'zabsolute': zurl,
            'zurl': z_object,
            'base_url': base_url,
            'zurl_counter': 0,
            'redirect_counter': 0,
            'top_zurls': None,
            'last_zurls': None,
            'site': Site.objects.get(id=settings.ZURL_REDIRECT_SITE_ID),
        },
        context_instance=RequestContext(request))

    return _set_browser_cookies(request, response)


@never_cache
def redirect_from_key(request, key):
    """ Demande de redirection vers une URL raccourcie
    """
    zurl, url, redirection_type = ZURL.get(key)
    source = QuotaCounter._getIP(request)
    if not zurl is None:
        log.info(
            u"[%s][k:%s/%s] redirection to %s" % (
                source, key, redirection_type, url)
        )

        direct = 1
        permredirect = (redirection_type == "301")
        RedirectLogs.add(zurl, source)

        if (not request.user.is_authenticated() and
            settings.ZURL_ANONYMOUS_ACTIVE_REDIRECT_IFRAME) or \
            (request.user.is_authenticated() and
                settings.ZURL_NONANONYMOUS_ACTIVE_REDIRECT_IFRAME):
            if (int(request.COOKIES.get(settings.ZURL_COOKIE_ROLLUP5, '1')) == 1):
                direct = 0
            response = render_to_response(
                "redirect.html",
                {
                    'target_title': zurl.title,
                    'zurl_target': url,
                    'direct': direct,
                    'displaytime': settings.ZURL_COOKIE_ROLLUP5_TIME
                },
                context_instance=RequestContext(request))
            return _set_browser_cookies(request, response, True)

        log.debug(u"direct redirect...")
        return redirect(url, permanent=permredirect)

    log.warning(u"[%s][k:%s] bad key" % (source, key))
    raise Http404


# AJAX request handlers ----------------------------------------------

@never_cache
def ajax_anonymous_url_set(request):
    """ Requête AJAX anonyme pour obtenir une adresse raccourcie
    """

    if request.GET:
        raise Http404

    quota_counter = QuotaCounter.set(
        settings.ZURL_ANONYMOUS_REQUEST_QUOTA_NAME,
        request
    )
    zurl = None
    base_url = z_object = None
    response = dict()
    if quota_counter.check():
        base_url = request.POST['base_url']
        validator = URLValidator()
        try:
            notvalid = False
            validator(base_url)
        except ValidationError:
            notvalid = True
        if not notvalid and not BlackDomain.is_blacklisted(base_url):
            base_key = request.POST['base_key']
            if len(base_key.strip()) == 0:
                base_key = None
            else:
                base_key = slugify(base_key)
            url_key = ZURL.set(base_url, request=request, key=base_key)
            if not url_key is None:
                current_site = Site.objects.get(id=settings.ZURL_REDIRECT_SITE_ID)
                log.info(u"[%s][k:%s] new key -> %s" % (
                    QuotaCounter._getIP(request), url_key, base_url))
                zurl = "http://%s/%s" % (current_site.domain, url_key)
                z_object = ZURL.objects.get(key=url_key)
                response['status'] = u'ok'
                response['message'] = u''
                response['zurl'] = {
                    'url': zurl,
                    'target': base_url,
                    'key': z_object.key,
                    'title': z_object.title,
                }
            else:
                response['status'] = u'nok'
                response['message'] = u'Impossible de créer votre raccourci !'
        else:
                response['status'] = u'nok'
                response['message'] = u'Impossible de créer votre raccourci : adresse non valide !'
    else:
        log.warning(u"[%s] quota exceed" % (QuotaCounter._getIP(request)))
        response['status'] = u'nok'
        response['message'] = u'Vous avez dépassé votre quota : essayez de nouveau plus tard...'

    json_data = json.dumps(response)
    return HttpResponse(json_data, mimetype="application/json")


@never_cache
def ajax_get_stats(request):
    json_data = """
    {
        "stats": {
            "zurl": {
                "total": %d
            },
            "router": {
                "total": %d
            }
        }
    }
    """ % (
        ZURL.objects.all().count() + settings.ZURL_HISTCOUNT_ZURL,
        RedirectLogs.objects.all().count() + settings.ZURL_HISTCOUNT_REDIRECT)
    return HttpResponse(json_data, mimetype="application/json")


@never_cache
def ajax_get_tops(request):
    json_data = json.dumps({
        'topview': [{
            'key': unicode(x.key),
            'title': unicode(x.title),
            'created_time': _json_mktime(x.created_time),
            'used_count': x.used_count} for x in _get_topn()],
        'toplast': [{
            'key': unicode(x.key),
            'title': unicode(x.title),
            'created_time': _json_mktime(x.created_time),
            'used_count': x.used_count} for x in _get_lastn()],
        'toptrend': [{
            'key': unicode(x.key),
            'title': unicode(x.title),
            'created_time': _json_mktime(x.created_time),
            'used_count': x.used_count} for x in _get_trendn()],
    })
    return HttpResponse(json_data, mimetype="application/json")


def _json_mktime(dt):
    return time.mktime(dt.timetuple())*1000

# Statistiques --------------------------------------------------------


def globales_last_month_stats(request):
    """obtenir les stats globales pour les 30 derniers jours"""

    # Routages ----

    data_rout_lastmonth_from_last_activ = list()
    data_rout_lastmonth_from_last_activ.append(['Date', 'Routage'])

    qsbrut = RedirectLogs.objects.all()
    if qsbrut.count() > 0:
        qss = qsstats.QuerySetStats(qsbrut, 'timestamp')

        lastres = qsbrut.aggregate(Max('timestamp'))
        last_activ = lastres['timestamp__max']
        first = last_activ - datetime.timedelta(days=30)
        res_lastmonth_from_last_activ = qss.time_series(first, last_activ)

        for row in res_lastmonth_from_last_activ:
            data_rout_lastmonth_from_last_activ.append(
                [str(row[0].date()), row[1]]
            )

    # Créations ----
    data_zurl_lastmonth_from_last_activ = list()
    data_zurl_lastmonth_from_last_activ.append(['Date', 'Création'])

    qsbrut = ZURL.objects.all()
    if qsbrut.count() > 0:
        qss = qsstats.QuerySetStats(qsbrut, 'created_time')

        lastres = qsbrut.aggregate(Max('created_time'))
        last_activ = lastres['created_time__max']
        first = last_activ - datetime.timedelta(days=30)
        res_lastmonth_from_last_activ = qss.time_series(first, last_activ)

        for row in res_lastmonth_from_last_activ:
            data_zurl_lastmonth_from_last_activ.append(
                [str(row[0].date()), row[1]]
            )

    response = render_to_response(
        "stats_globales.html",
        {
            'data_zurl_lastmonth_from_last_activ': data_zurl_lastmonth_from_last_activ,
            'data_rout_lastmonth_from_last_activ': data_rout_lastmonth_from_last_activ,
        },
        context_instance=RequestContext(request))

    return _set_browser_cookies(request, response)


def get_key_redirects_last_month_stats(request, key, ajax=False):
    """obtenir les stats pour un raccourci donné pour les 30 derniers jours"""

    data_lastmonth_from_last_activ = list()
    data_lastmonth_from_last_activ.append(['Date', 'Routage'])

    zurl = ZURL.objects.get(key=key)
    qsbrut = RedirectLogs.objects.filter(zurl__key=key)
    if qsbrut.count() > 0:
        qss = qsstats.QuerySetStats(qsbrut, 'timestamp')

        lastres = qsbrut.aggregate(Max('timestamp'))
        last_activ = lastres['timestamp__max']
        first = last_activ - datetime.timedelta(days=30)
        res_lastmonth_from_last_activ = qss.time_series(first, last_activ)

        for row in res_lastmonth_from_last_activ:
            data_lastmonth_from_last_activ.append(
                [str(row[0].date()), row[1]]
            )

    if ajax:
        json_data = json.dumps({
            'zurl': {
                'key': unicode(zurl.key),
                'title': unicode(zurl.title),
                'created_time': _json_mktime(zurl.created_time),
                'used_count': zurl.used_count
            },
            'absolute': _get_key_redirect_url(zurl.key),
            'data_lastmonth_from_last_activ': data_lastmonth_from_last_activ
        })
        return HttpResponse(json_data, mimetype="application/json")

    response = render_to_response(
        "stats_key.html",
        {
            'zurl': zurl,
            'absolute': _get_key_redirect_url(zurl.key),
            'data_lastmonth_from_last_activ': data_lastmonth_from_last_activ,
        },
        context_instance=RequestContext(request))

    return _set_browser_cookies(request, response)

# EXPERIMENTAL --------------------------------------------------------


def update_site_preview(
        key,
        screen=1280,
        width=280,
        height=320):
    """Creation d'une image de prévisualisation du site cible"""
    api_url_base = "http://api.webthumbnail.org/?format=png&width=%d&height=%d&screen=%d&url=%s"
    zurl = ZURL.objects.get(key=key)
    api_url = api_url_base % (width, height, screen, zurl.url)
    api_url_status = api_url.replace('?', '?action=get-status&')
    log.debug(u"site preview request %s" % (api_url))
    try:
        result = "requested"
        maxtime = 0
        requests.get(api_url)
        while not result == '"finished"':
            log.debug(u"waiting for preview... (status:%s)" % (result))
            time.sleep(2)
            maxtime += 2
            r = requests.get(api_url_status)
            if not r.status_code == 200 or maxtime > 10:
                return False
            result = r.content
        log.debug(u"... ready. (status:%s)" % (result))
        r = requests.get(api_url)
        filetype = r.headers['content-type'].split('/')
        if r.status_code == 200 and filetype[0] == 'image' and filetype[1] == 'png':
            img = open(
                _get_img_attributes(
                    key, dirname="preview", ext=filetype[1]),
                "wb")
            img.write(r.content)
            img.close()
            return True
    except Exception:
        return False
    return False


def update_site_QRcode(key, width=160, height=160):
    """Création d'un QRcode pour l'url raccourcie"""
    api_url_base = "http://chart.apis.google.com/chart?cht=qr&chs=%dx%d&chl=%s"
    zurl_url = _get_key_redirect_url(key)
    api_url = api_url_base % (width, height, zurl_url)
    log.debug(u"requete QRcode raccourci %s" % (zurl_url))
    try:
        r = requests.get(api_url)
        filetype = r.headers['content-type'].split('/')
        if r.status_code == 200 and filetype[0] == 'image':
            img = open(
                _get_img_attributes(
                    key, dirname="qrcode", ext=filetype[1]),
                "wb")
            img.write(r.content)
            img.close()
            return True
    except Exception:
        return False
    return False


def _get_img_attributes(key, dirname="preview", ext="png"):
    img_filename = "%s.%s" % (key, ext)
    img_filedir = os.path.join(settings.MEDIA_ROOT, dirname)
    img_filepath = os.path.join(img_filedir, img_filename)
    return img_filepath

# Utilities ----------------------------------------------------------


def _get_key_redirect_url(key):
    current_site = Site.objects.get(id=settings.ZURL_REDIRECT_SITE_ID)
    zurl = "http://%s/%s" % (current_site.domain, key)
    return zurl


def _get_topn(n=10):
    zs = ZURL.objects.filter(private=False).order_by('-used_count')
    zs = zs.exclude(title=None)
    return zs[:n]


def _get_lastn(n=10):
    zs = ZURL.objects.filter(private=False).order_by('-created_time')
    zs = zs.exclude(title=None)
    return zs[:n]


def _get_trendn(n=10):
    tslimit = datetime.datetime.now() - datetime.timedelta(days=7)
    zs = ZURL.objects.filter(private=False, created_time__gte=tslimit)
    zs = zs.exclude(title=None).order_by('-used_count')
    return zs[:n]
