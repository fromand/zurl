#-*- coding: utf-8 -*-

import datetime
import logging
from optparse import make_option

from django.core.management.base import BaseCommand
from django.db.models import Max

from zurl_api.models import *
from zurl_api.views import _get_topn

log = logging.getLogger('zurl_api')


class Command(BaseCommand):

    help = u"Relevé des compteurs et Nettoyage de la BD"

    option_list = BaseCommand.option_list + (
        make_option(
            '--delete',
            action='store_true',
            dest='delete',
            default=False,
            help=u'Nettoyage réel de la BD'),
    )

    retaindays = 30

    def handle(self, *args, **options):

        log.info(Command.help)

        if options['delete']:
            log.info(u'ATTENTION : SUPPRESSION REELLE DANS BASE DE DONNEES !')

        log.info(u'Nbre de raccourcis : %d' % (ZURL.objects.all().count()))
        log.info(u'Nbre de redirections : %d' % (RedirectLogs.objects.all().count()))

        dlim = datetime.datetime.now() - datetime.timedelta(days=Command.retaindays)

        # Liste des raccourcis avec dernière redirection < limite de date 
        zurl_2_delete = RedirectLogs.objects.order_by('zurl').values('zurl__key').annotate(rmax=Max('timestamp'))
        zurl_2_delete = zurl_2_delete.filter(rmax__lt=dlim)

        # Obtenir le top 10
        top10 = _get_topn()

        zurls_count = 0
        redirects_count = 0

        for zurldict in zurl_2_delete:

            zurl = ZURL.objects.get(key=zurldict['zurl__key'])

            # fait partie du top10 ? si non, continue
            if zurl in top10:
                continue

            # comptage des redirections et suppression
            redirects = RedirectLogs.objects.filter(zurl=zurl)
            redirects_count += redirects.count()
            if options['delete']:
                redirects.delete()

            # suppression du raccourci
            zurls_count += 1
            if options['delete']:
                zurl.delete()

        log.info(u'Nombre de jours de rétention : %d' % (Command.retaindays))
        if not options['delete']:
            log.info(u'Nbre de raccourcis à supprimer : %d' % (zurls_count))
            log.info(u'Nbre de redirections à supprimer : %d' % (redirects_count))
        else:
            log.info(u'Nbre de raccourcis supprimés : %d' % (zurls_count))
            log.info(u'Nbre de redirections supprimées : %d' % (redirects_count))
