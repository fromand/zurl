#-*- coding: utf-8 -*-

from django.utils import timezone
from django.test import TestCase

from shorturl import encode, enbase, debase, decode
from models import *


class ShortUrlTest(TestCase):

    def test_all(self):
        for a in range(0, 10000000, 373):
            b = encode(a)
            c = enbase(b)
            d = debase(c)
            e = decode(d)
            self.assertEqual(a, e)
            self.assertEqual(b, d)


class ZURLTest(TestCase):

    def setUp(self):
        ZURL.objects.create(
            url="http://www.google.com",
            key="testkey",
        )

    def test_is_anonymous(self):
        zurl = ZURL.objects.get(key="testkey")
        user = User.objects.create_user(
            'temporary', 'temporary@gmail.com', 'temporary')

        self.assertTrue(zurl.is_anonymous)

        zurl.created_user = user
        zurl.save()
        self.assertFalse(zurl.is_anonymous)

    def test_is_timeout(self):
        zurl = ZURL.objects.get(key="testkey")

        self.assertFalse(zurl.is_timeout)  # w/o expiration time

        zurl.expiration_time = timezone.now() + \
            timezone.timedelta(seconds=3600)
        self.assertFalse(zurl.is_timeout)

        zurl.expiration_time = timezone.now() - \
            timezone.timedelta(seconds=3600)
        self.assertTrue(zurl.is_timeout)

    def test_is_over(self):
        zurl = ZURL.objects.get(key="testkey")

        self.assertFalse(zurl.is_over)  # w/o limit setUp

        zurl.usage_max = 1
        zurl.used_count = 0
        self.assertFalse(zurl.is_over)

        zurl.used_count = 1
        self.assertFalse(zurl.is_over)

        zurl.used_count = 2
        self.assertTrue(zurl.is_over)

    def test_is_protected(self):
        zurl = ZURL.objects.get(key="testkey")

        self.assertFalse(zurl.is_protected)  # w/o password set

        zurl.password_sha1 = "passwordsha1"
        self.assertTrue(zurl.is_protected)

        zurl.password_sha1 = " "
        self.assertFalse(zurl.is_protected)

        zurl.password_sha1 = None
        self.assertFalse(zurl.is_protected)

    def test_save(self):
        zurl = ZURL.objects.get(key="testkey")

        zurl.key = None
        zurl.save()
        self.assertEqual(zurl.id, 1)
        self.assertEqual(zurl.key, "ZwKB")

        zurl.key = u"  "
        zurl.save()
        self.assertEqual(zurl.id, 1)
        self.assertEqual(zurl.key, "ZwKB")

        zurl.key = "testkey"
        zurl.save()
        self.assertEqual(zurl.key, "testkey")

    def test_get(self):
        zurl = ZURL.objects.get(key="testkey")

        z, g1, redir_type = ZURL.get("testkey")
        self.assertEqual(z, zurl)
        self.assertEqual(g1, zurl.url)
        self.assertEqual(redir_type, zurl.redirection_type)

        self.assertEqual(ZURL.get("notakey"), (None, None, None))

        zurl.password_sha1 = "test_sha1"
        zurl.save()
        self.assertEqual(ZURL.get(
            "testkey", "test_sha1"), (zurl, zurl.url, zurl.redirection_type,))
        self.assertEqual(ZURL.get(
            "testkey", "test_bad_sha1"), (None, None, None,))
        zurl.password_sha1 = None
        zurl.save()

        zurl.expiration_time = timezone.now() - \
            timezone.timedelta(seconds=3600)
        zurl.save()
        self.assertEqual(ZURL.get("testkey"), (None, None, None))
        zurl.expiration_time = timezone.now() + \
            timezone.timedelta(seconds=3600)
        zurl.save()
        self.assertEqual(ZURL.get("testkey"),
                         (zurl, zurl.url, zurl.redirection_type))
        zurl.expiration_time = None
        zurl.save()

        zurl.used_count = 0
        zurl.usage_max = 2
        zurl.save()
        z, u, r = ZURL.get("testkey")
        zurl = ZURL.objects.get(key="testkey")
        self.assertEqual(zurl.used_count, 1)
        z, u, r = ZURL.get("testkey")
        zurl = ZURL.objects.get(key="testkey")
        self.assertEqual(zurl.used_count, 2)
        z, u, r = ZURL.get("testkey")
        zurl = ZURL.objects.get(key="testkey")
        self.assertEqual(zurl.used_count, 3)
        z, u, r = ZURL.get("testkey")
        self.assertEqual(z, None)

    def test_set(self):
        key = ZURL.set("http://github.com")
        self.assertFalse(key is None)
        z = ZURL.objects.get(key=key)
        self.assertEqual(key, z.key)
        self.assertEqual(z.private, False)
        self.assertEqual(z.password_sha1, None)
        self.assertEqual(z.expiration_time, None)
        self.assertEqual(z.usage_max, 0)

        key = ZURL.set("http://github.com")
        self.assertEqual(key, z.key)
        z.delete()

        key = ZURL.set("http://github.com", key="testkey3")
        self.assertEqual(key, "testkey3")
        z = ZURL.objects.get(key="testkey3")
        z.delete()

        tsexpir = timezone.now() + timezone.timedelta(seconds=3600)
        key = ZURL.set("http://github.com",
                       key="testkey3", expiration_time=tsexpir)
        self.assertEqual(key, "testkey3")
        z = ZURL.objects.get(key="testkey3")
        self.assertEqual(z.expiration_time, tsexpir)
        z.delete()

        pwd = "password"
        key = ZURL.set("http://github.com", key="testkey3", password_sha1=pwd)
        self.assertEqual(key, "testkey3")
        z = ZURL.objects.get(key="testkey3")
        self.assertEqual(z.password_sha1, pwd)
        z.delete()

        private = True
        key = ZURL.set("http://github.com", key="testkey3", private=private)
        self.assertEqual(key, "testkey3")
        z = ZURL.objects.get(key="testkey3")
        self.assertEqual(z.private, private)
        z.delete()

        redirection_type = "301"
        key = ZURL.set("http://github.com",
                       key="testkey3", redirection_type=redirection_type)
        self.assertEqual(key, "testkey3")
        z = ZURL.objects.get(key="testkey3")
        self.assertEqual(z.redirection_type, redirection_type)
        z.delete()

    def test_logs(self):
        RedirectLogs.objects.all().delete()
        z = ZURL.objects.get(key="testkey")
        RedirectLogs.add(z, "127.0.0.1")
        l = RedirectLogs.objects.all()
        self.assertEqual(l[0].zurl, z)
        self.assertEqual(l[0].source_IP, "127.0.0.1")


