#-*- coding: utf-8 -*-

from django.conf.urls import *

urlpatterns = patterns(
    'zurl_api.views',

    # Redirection des raccourcis ------------
    (r'^(?P<key>[a-zA-Z0-9\-]+)$', 'redirect_from_key'),

    # Détails d'un raccourci ------------
    (r'^(?P<key>[a-zA-Z0-9\-]+)\+$', 'get_key_redirects_last_month_stats'),

    # création via favori
    (r'^\+', 'anonymous_url_set', {'direct': True}),

    # requêtes ajax
    (r'^zurl/api/ajax/stats$', 'ajax_get_stats', ),
    (r'^zurl/api/ajax/tops$', 'ajax_get_tops', ),
    (r'^zurl/api/ajax/details/(?P<key>[a-zA-Z0-9]+)$',
        'get_key_redirects_last_month_stats',
        {'ajax': True}),
    (r'^zurl/api/ajax/anonymous/set$', 'ajax_anonymous_url_set', ),

    # Statistiques globales ------------
    (r'^zurl/stats$', 'globales_last_month_stats'),

    # Tops ----
    (r'^zurl/tops$', 'tops_view'),

)
