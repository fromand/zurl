#-*- coding: utf-8 -*-

import time

from django.utils import timezone
from django.test import TestCase
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.test.client import Client
from django.conf import settings

from models import *


class QuotaTest(TestCase):

    def setUp(self):
        Quota.objects.create(
            name="test", limit=10, cycle_seconds=5)

        quota = Quota.objects.get(name="test")
        QuotaCounter.objects.create(
            key="test", quota=Quota.objects.get(name="test"),
            anonymous=True, user=None, next_cycle=timezone.now() +
                timezone.timedelta(seconds=quota.cycle_seconds),
            counter=0
        )
        quota = Quota.objects.create(
            name=settings.ZURL_ANONYMOUS_REQUEST_QUOTA_NAME,
            limit=10, cycle_seconds=5
        )
        Site.objects.create(
            id=settings.ZURL_REDIRECT_SITE_ID,
            domain="127.0.0.1", name="test"
        )

    def test_counter_timeout(self):
        counter = QuotaCounter.objects.get(key="test")

        self.assertEqual(counter.is_timeout, False)

        counter.next_cycle = timezone.now() - timezone.timedelta(seconds=3600)
        self.assertEqual(counter.is_timeout, True)

        counter.reset()
        self.assertEqual(counter.is_timeout, False)

    def test_counter_over(self):
        counter = QuotaCounter.objects.get(key="test")

        counter.reset()
        self.assertEqual(counter.is_over, False)

        counter.counter = 10
        self.assertEqual(counter.is_over, False)

        counter.counter = 11
        self.assertEqual(counter.is_over, True)

    def test_counter_reset(self):
        counter = QuotaCounter.objects.get(key="test")

        counter.reset()
        self.assertEqual(counter.counter, 0)

        counter.counter += 1
        self.assertEqual(counter.counter, 1)

        counter.reset(value=10)
        self.assertEqual(counter.counter, 10)

        t1 = timezone.now()
        counter.reset()
        t2 = timezone.now()
        self.assertTrue(
            counter.next_cycle > t1 +
            timezone.timedelta(seconds=counter.quota.cycle_seconds)
        )
        self.assertTrue(
            counter.next_cycle < t2 +
            timezone.timedelta(seconds=counter.quota.cycle_seconds)
        )

    def test_counter_check(self):
        counter = QuotaCounter.objects.get(key="test")
        counter.reset(value=8)

        res = counter.check()
        self.assertTrue(res)
        self.assertEqual(counter.counter, 9)

        res = counter.check()
        self.assertTrue(res)
        self.assertEqual(counter.counter, 10)

        counter.check()
        res = counter.check()
        self.assertFalse(res)

        time.sleep(5)
        res = counter.check()
        self.assertTrue(res)
        self.assertEqual(counter.counter, 1)

    def test_counter_set(self):

        c = Client()
        response = c.post(
            '/zurl/api/anonymous/set',
            {
                'base_url': 'http://www.google.com',
                'base_key': ''
            }
        )

        counter = QuotaCounter.set("test", response.request)

        self.assertEqual(counter.key, "127.9.9.9")
        self.assertTrue(counter.anonymous)
        self.assertEqual(counter.counter, 0)

        # user = User.objects.create_user(
        #     'temporary', 'temporary@gmail.com', 'temporary')

        # counter = QuotaCounter.set("test", user, "127.0.0.1")
        # self.assertEqual(counter.key, "temporary")
        # self.assertFalse(counter.anonymous)
        # self.assertEqual(counter.counter, 0)

    def test_counter_staticmethod_with_request(self):
        c = Client()

        response = c.post(
            '/zurl/api/anonymous/set',
            {
                'base_url': 'http://www.google.com',
                'base_key': ''
            }
        )
        
        counter = QuotaCounter.set(
            settings.ZURL_ANONYMOUS_REQUEST_QUOTA_NAME,
            response.request)

        response = c.post(
            '/zurl/api/badurl/test',
            {
                'base_url': 'http://www.google.fr',
                'base_key': ''
            }
        )
        self.assertEqual(response.status_code, 404)

        response = c.post(
            '/zurl/api/anonymous/set',
            {
                'base_url': 'http://www.google.fr',
                'base_key': ''
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(QuotaCounter.is_allowed(response.request))

        counter.reset(value=11)
        response = c.post(
            '/zurl/api/anonymous/set',
            {
                'base_url': 'http://www.google.com',
                'base_key': ''
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(QuotaCounter.is_allowed(response.request))

        self.assertEqual(QuotaCounter._getkey(response.request), "127.9.9.9")
        self.assertEqual(QuotaCounter._getIP(response.request), "127.9.9.9")
