#-*- coding: utf-8 -*-

import logging

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


log = logging.getLogger('quota')


class Quota(models.Model):
    """ Table de référence des quotas
    """
    name = models.CharField(
        max_length=32, unique=True, db_index=True, verbose_name=u"name")
    limit = models.IntegerField(verbose_name=u"count limit", default=1000)
    cycle_seconds = models.IntegerField(
        verbose_name=u"cycle (seconds)", default=3600*24)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = u"quotas"
        verbose_name = u"quota"
        ordering = ["name", ]


class QuotaCounter(models.Model):
    """ Compteur de quota
    """
    key = models.CharField(max_length=128, db_index=True, verbose_name=u"key")
    quota = models.ForeignKey(
        Quota, related_name="quota_related",
        db_index=True, verbose_name=u"quota")
    anonymous = models.BooleanField(
        verbose_name=u'anonyme', null=False, blank=True, default=True)
    user = models.ForeignKey(
        User, null=True, blank=True, related_name="quotaowner_related",
        db_index=True, verbose_name=u"owner")
    next_cycle = models.DateTimeField(
        blank=True, null=True, verbose_name=u"next cycle", default=None)
    counter = models.IntegerField(verbose_name=u"counter", default=0)
    is_blacklisted = models.BooleanField(
        verbose_name=u'blacklisted', null=False, blank=True, default=False)

    def __unicode__(self):
        return u"%s:%s" % (self.quota, self.key)

    class Meta:
        verbose_name_plural = u"Quota counters"
        verbose_name = u"quota counter"
        ordering = ["quota", "key", ]
        unique_together = ("quota", "key")

    @property
    def is_over(self):
        """ Contrôle du dépassement du quota (volume)
            Renvoi True si dépassement
        """
        if self.counter <= self.quota.limit:
            return False
        return True

    @property
    def is_timeout(self):
        """ Contrôle de la fin du cycle de contrôle du volume
            Renvoi True si fin de cycle atteinte
        """
        now = timezone.now()
        if not self.next_cycle is None:
            if self.next_cycle < now:
                return True
        return False

    def reset(self, value=0):
        """ Reinitialise le Compteur
            Le compteur est initialisé avec la value spécifié ou 0 par défaut
        """
        self.counter = value
        self.next_cycle = timezone.now() + \
            timezone.timedelta(seconds=self.quota.cycle_seconds)
        self.save()

    def check(self):
        """ Vérifie l'ouverture du quota et incrémente le compteur.
            Renvoi vrai si quota ouvert.
        """
        if self.is_blacklisted:
            return False
        if self.is_timeout:
            self.reset(value=1)
            return True
        elif not self.is_over:
            self.counter += 1
            self.save()
            return True
        return False

    @staticmethod
    def set(quota_name, request):
        """ Création d'un compteur de quota
            Si un user est spécifié, la clé du compteur est l'utilisateur.
            Si le user est None, une adresse IP doit être spécifiée.
        """

        anonymous = True
        if 'user' in request and not request.user.is_anonymous():
            anonymous = False
        quota = Quota.objects.get(name=quota_name)
        counter, created = QuotaCounter.objects.get_or_create(
            key=QuotaCounter._getkey(request),
            quota=quota)
        if created:
            log.debug(u"New quota set (%s)" % (counter))
            counter.anonymous = anonymous
            if not anonymous:
                counter.user = request.user
            else:
                counter.user = None
            counter.reset()
        else:
            log.debug(u"Quota already set (%s)" % (counter))
        return counter

    @staticmethod
    def is_allowed(request):
        """ Vérifie que la request est autorisée (sans compteur de quota fermé)
            Si request anonyme, c'est l'IP qui sert de clé. Si user login,
            le user est utilisé comme clé du compteur.
            Renvoi Vrai si aucun quota correspondant fermé
        """
        key = QuotaCounter._getkey(request)
        counters = QuotaCounter.objects.filter(key=key)
        for counter in counters:
            if not counter.check():
                log.warning(u"Quota is over (%s)" % (counter))
                return False
        return True

    @staticmethod
    def _getkey(request, forceIP=False):
        """ Renvoi le username si requête non anonyme,
            sinon la valeur du cookie 'browser uuid' si disponible
            sinon l'adresse IP
            Si forceIP is True, renvoi systématiquement l'adresse IP
        """

        if forceIP or request is None:
            return QuotaCounter._getIP(request)

        try:
            if not request.user.is_anonymous():
                return request.user.username
        except Exception:
            pass

        try:
            user_uuid = request.COOKIES.get(settings.ZURL_COOKIE_BROWSER_UUID, None)
            if user_uuid is not None:
                return user_uuid
        except Exception:
            pass

        return QuotaCounter._getIP(request)

    @staticmethod
    def _getIP(request):
        """ Renvoi l'adresse IP de la request
        """
        try:
            return request.META.get('HTTP_X_FORWARDED_FOR', False) or \
                request.META.get('REMOTE_ADDR', '127.9.9.9')
        except Exception:
            return '127.9.9.9'

