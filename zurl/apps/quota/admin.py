#-*- coding: utf-8 -*-

from django.contrib import admin

from models import *


class QuotaAdmin(admin.ModelAdmin):

    list_display = ['id', 'name', 'limit', 'cycle_seconds', ]
    list_display_links = ['id', 'name', ]
    search_fields = ['name', ]
    list_filter = ['name', ]
    ordering = ['name', ]
    actions = ['delete_selected', ]


class QuotaCounterAdmin(admin.ModelAdmin):

    list_display = [
        'id', 'quota', 'key', 'anonymous', 'user',
        'next_cycle', 'counter', 'is_over', 'is_timeout',
        'is_blacklisted'
    ]
    list_display_links = ['id', 'key', ]
    search_fields = ['quota__name', 'key', ]
    list_filter = ['quota', 'anonymous', 'is_blacklisted']
    ordering = ['quota', 'key', ]
    actions = ['delete_selected', ]


admin.site.register(Quota, QuotaAdmin)
admin.site.register(QuotaCounter, QuotaCounterAdmin)
