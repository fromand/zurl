#-*- coding: utf-8 -*-

import os

import dj_database_url

from settings import INSTALLED_APPS


DEBUG = (str(os.getenv("DEBUG")) == "True")

ZURL_ANONYMOUS_REQUEST_QUOTA_NAME = os.getenv("ZURL_ANONYMOUS_REQUEST_QUOTA_NAME")
ZURL_REDIRECT_SITE_ID = int(os.getenv("ZURL_REDIRECT_SITE_ID"))

ZURL_COOKIE_ROLLUP5_MAX = int(os.getenv("ZURL_COOKIE_ROLLUP5_MAX"))
ZURL_COOKIE_ROLLUP5_TIME = int(os.getenv("ZURL_COOKIE_ROLLUP5_TIME"))

ZURL_HISTCOUNT_ZURL = int(os.getenv("ZURL_HISTCOUNT_ZURL"))
ZURL_HISTCOUNT_REDIRECT = int(os.getenv("ZURL_HISTCOUNT_REDIRECT"))

LANGUAGE_CODE = "fr"
USE_I18N = True

ADMINS = (
    ('Fabrice Romand', 'fabrice.romand@gmail.com'),
)
MANAGERS = ADMINS

TEST_RUNNER = 'django.test.simple.DjangoTestSuiteRunner'

# Database positionnée selon var. env. DATABASE_URL
DATABASES = dict()
DATABASES['default'] = dj_database_url.config(
    default='postgres://localhost')

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

# SMTP ----
EMAIL_HOST_USER = os.environ['SENDGRID_USERNAME']
EMAIL_HOST= 'smtp.sendgrid.net'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_PASSWORD = os.environ['SENDGRID_PASSWORD']
# ----

ALLOWED_HOSTS = ['*', ]

PAGES_MENU_SHOW_ALL = False
