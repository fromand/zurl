#-*- coding: utf-8 -*-

__application_info__ = {
    'name': u'jVai.fr',
    'version': u'1.3.1',
    'description': u"URL minimizer services",
    'author': u'Fabrice Romand',
    'author_email': u'fabrice.romand@gmail.com',
    'author_url': u'http://jvai.fr',
    'maintainer': u'Fabrice Romand',
    'maintainer_email': u'fabrice.romand@gmail.com',
    'url': u'http://jvai.fr',
    'download_url': u'',
    'copyright': u'© 2014',
    'license': u'GPL v2',
    'license_url': u'http://fsffrance.org/gpl/gpl-fr.fr.html'
}
