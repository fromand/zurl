============
ZURL ROADMAP
============


Version 1.3.0
-------------

- Choix du type de redirection (permanente, temporaire)
- Mode de redirection : direct ou avec previsualisation
  La prévisualisation permet de contrôler le site distant avant d'y accéder.
  -> Pub possible !
- Redirection limitée dans le temps (implique type temporaire)
- Redirection restreinte par mot de passe (donc)
- Redirection limitée en nombre


Version 2.0.0
-------------

- prévisu du site cible
- Création de compte utilisateur nominatif
- Job régulier de contrôle accessibilité pages cibles
(suppression si n'existe plus)