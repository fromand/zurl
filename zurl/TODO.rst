TODO
====


Avant release version 1.0
-------------------------

- Communiquer sur site
- Voir roadmap
- Page décrivant les outils
(bookmark de création directe, controle d'un raccourci, statistiques d'un raccourci, creation d'un QRCode, Liste des derniers raccourcis créés,
Liste des raccourcis les plus consultés, API, ...)
- Formulaire de contact
- Blog d'avancement du projet (fonctionnalités, bugs, corrections, ...)


NOTES
=====

Bookmark pour raccourci direct depuis la page à raccourcir
----------------------------------------------------------
snippet:
javascript:(function(){window.location="http://jvai.fr/+?base_url="+encodeURIComponent(window.location.href);})()


Copier/Coller
-------------

Automatic copying to clipboard may be dangerous, therefore most browsers (except IE) make it very difficult. Personally, I use the following simple trick:

function copyToClipboard(text) {
  window.prompt("Copie du raccourci dans le presse-papier: Ctrl+C, Enter", text);
}

The user is presented with the prompt box, where the text to be copied is already selected. Now it's enough to press Ctrl+C and Enter (to close the box) -- and voila!

Now the clipboard copy operation is SAFE, because the user does it manually (but in a pretty straightforward way). Of course, works in all browsers.


Ajouter un bookmark depuis un bouton sur une page
--------------------------------------------------
JavaScript:
<script type="text/javascript">
  $(document).ready(function() {
    $("#bookmarkme").click(function() {
      if (window.sidebar) { // Mozilla Firefox Bookmark
        window.sidebar.addPanel(location.href,document.title,"");
      } else if(window.external) { // IE Favorite
        window.external.AddFavorite(location.href,document.title); }
      else if(window.opera && window.print) { // Opera Hotlist
        this.title=document.title;
        return true;
  }
});
HTML:
<a id="bookmarkme" href="#" rel="sidebar" title="bookmark this page">Bookmark This Page</a>
IE will show an error if you don't run it off a server (it doesn't allow JavaScript bookmarks via JavaScript when viewing it as a file://....


HTML to prévisu.
----------------
Voir http://www.shrinktheweb.com
http://code.google.com/p/wkhtmltopdf/
http://webthumb.bluga.net/home
https://github.com/AdamN/python-webkit2png


Intégration pub
---------------

Voir http://www.zeropub.net/